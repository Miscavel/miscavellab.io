import HomeSVG from '../shared/svg/HomeSVG';
import DashboardSVG from '../shared/svg/DashboardSVG';
import ProfileSVG from '../shared/svg/ProfileSVG';

function NavigationBar() {
  return (
    <div className='navigation-bar'>
      <div className='navigation-bar--tab'>
        <div className='navigation-bar--tab-icon'>
          <HomeSVG></HomeSVG>
        </div>
      </div>
      <div className='navigation-bar--tab'>
        <a href='https://gitlab.com/Miscavel/' target='_blank' rel='noreferrer'>
          <div className='navigation-bar--tab-icon'>
            <DashboardSVG></DashboardSVG>
          </div>
        </a>
      </div>
      <div className='navigation-bar--tab'>
        <a href='https://www.linkedin.com/in/ricky-sunartio/' target='_blank' rel='noreferrer'>
          <div className='navigation-bar--tab-icon'>
            <ProfileSVG></ProfileSVG>
          </div>
        </a>
      </div>
    </div>
  )
}

export default NavigationBar;