import ProjectItem from './ProjectItem';
import { useWindowSize } from '../../util/hooks';

function ProjectList({ tags, projects, onProjectSortChange }) {
  useWindowSize();
  
  return (
    <div className='project-list'>
      <div className='project-list--sort'>
        <select
          onChange={ onProjectSortChange }
        >
          <option value="date_desc">Date Created (newest first)</option>
          <option value="date_asc">Date Created (oldest first)</option>
          <option value="a_to_z">Title (A - Z)</option>
          <option value="z_to_a">Title (Z - A)</option>
        </select>
      </div>
      <div className='project-list--content'>
        {
          projects.map(function(project, index) {
            return (
              <ProjectItem
                tags={ tags }
                project={ project }
                key={ index }
              >
              </ProjectItem>
            );
          })
        }
      </div>
    </div>
  )
}

export default ProjectList;