function Tooltip({ text }) {
  return (
    <>
      <div className='tooltip--arrow-up'>
      </div>
      <div className='tooltip'>
        { text }
      </div>
    </>
  )
}

export default Tooltip;