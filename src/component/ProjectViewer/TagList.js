import Tag from "./Tag";
import { getAllTag } from './config';
import { useState } from "react";

function TagList({ tags, onTagRemoveClick, onTagsClear }) {
  const [ allTag ] = useState(getAllTag());

  const activeTagsCount = tags.reduce(function(res, tag) {
    res += tag.checked ? 1 : 0;
    return res;
  }, 0);

  return (
    <div className='tag-list'>
      <div className='tag-list--label-group'>
        <div 
          className={`tag-list--label ${activeTagsCount > 0 ? 'active' : ''}`}
          onClick={ onTagsClear }
        >
          <svg 
            className='tag-list--label-svg'
            xmlns="http://www.w3.org/2000/svg" 
            viewBox="0 0 32 32" 
            width="10" 
            role="presentation" 
            alt="" 
            data-testid="XIcon" 
            size="10" 
            color="currentColor"
          >
            <path fill="currentColor" d="M32 4.53L27.47 0 16 11.47 4.53 0 0 4.53 11.47 16 0 27.47 4.53 32 16 20.53 27.47 32 32 27.47 20.53 16 32 4.53z">
            </path>
          </svg>
          <div className='tag-list--label-text'>
            Clear tags
          </div>
        </div>
      </div>
      <div className='tag-list--content'>
        { 
          tags.map(function(tag, index) {
            const { checked } = tag;
            return (
              <Tag
                tag={ tag }
                shouldShow={ checked }
                onTagRemoveClick={ onTagRemoveClick }
                isRemovable={ true }
                key={ index }
              > 
              </Tag>
            );
          })
        }
        {
          (activeTagsCount === 0) &&
          <Tag
            tag={ allTag }
            shouldShow={ true }
            isRemovable={ false }
          >
          </Tag>
        }
      </div>
    </div>
  );
}

export default TagList;