import { getPublicImageUrl } from "../../util/asset";

function TagSelectorItem({ tag, onTagClick }) {
  const { name, checked, icon, isInFilter } = tag;
  const checkIconUrl = checked ?
    getPublicImageUrl('check.svg') :
    '';
  return (
    <>
      {
        isInFilter &&
        <div 
          className='tag-selector--item'
          onClick={function() {
            onTagClick(tag);
          }}
        >
          <div className='tag-selector--item-check-wrapper'>
            <img className='tag-selector-item--check' src= { checkIconUrl }></img>
          </div>
          <div className='tag-selector--item-icon-wrapper'>
            <img className='tag-selector--item-icon' src={ icon }></img>
          </div>
          <div className='tag-selector--item-name'>
            { name }
          </div>
        </div>
      }
    </>
  )
}

export default TagSelectorItem;