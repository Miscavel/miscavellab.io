import TagSelector from './TagSelector';
import { getDefaultProjects, getDefaultTags } from './config';
import { useState } from 'react';
import TagList from './TagList';
import ProjectList from './ProjectList';

function ProjectViewer() {
  const [ tags, setTags ] = useState(getDefaultTags());
  const [ showTags, setShowTags ] = useState(false);

  const [ projects, setProjects ] = useState(getDefaultProjects());

  function onTagClick(tag) {
    tag.checked = !tag.checked;
    updateActiveTags();
  }

  function onTagRemoveClick(tag) {
    tag.checked = false;
    updateActiveTags();
  }

  function onTagsClear() {
    tags.forEach(function(tag) {
      tag.checked = false;
    });
    updateActiveTags();
  }

  function updateActiveTags() {
    setTags([...tags]);
    updateActiveProjects();
  }

  function updateActiveProjects() {
    let showAllProjects = true;
    const activeTagKeysMap = {};
    tags.forEach(function(tag) {
      const { checked, key } = tag;
      if (checked) {
        showAllProjects = false;
        activeTagKeysMap[key] = 1;
      }
    });
    projects.forEach(function(project) {
      const { tagList } = project;
      const isInFilter = tagList.reduce(function(res, tag) {
        return res || activeTagKeysMap[tag];
      }, showAllProjects);
      project.isInFilter = isInFilter;
    });
    setProjects([...projects]);
  }
  
  function onTagSearch(event) {
    const search = event.target.value.toLowerCase();
    tags.forEach(function(tag) {
      const { name } = tag;
      const isInFilter = name.toLowerCase().includes(search);
      tag.isInFilter = isInFilter;
    });
    setTags([...tags]);
  }

  function onProjectSortChange(event) {
    const sortMode = event.target.value;
    let sortedProjects;
    switch(sortMode) {
      case 'date_desc': {
        sortedProjects = projects.sort(function(a, b) {
          return b.date - a.date;
        });
        break;
      }

      case 'date_asc': {
        sortedProjects = projects.sort(function(a, b) {
          return a.date - b.date;
        });
        break;
      }

      case 'a_to_z': {
        sortedProjects = projects.sort(function(a, b) {
          return a.title.toLowerCase() < b.title.toLowerCase() ? -1 : 1;
        });
        break;
      }

      case 'z_to_a': {
        sortedProjects = projects.sort(function(a, b) {
          return b.title.toLowerCase() < a.title.toLowerCase() ? -1 : 1;
        });
        break;
      }

      default: {
        sortedProjects = projects.sort(function(a, b) {
          return b.date - a.date;
        });
        break;
      }
    }
    setProjects([...sortedProjects]);
  }

  function preventDefaultOnMouseDown(event) {
    // Prevent default so that clicking on tag selector won't trigger input blur
    event.preventDefault();
  }

  return (
    <div className='project-viewer'>
      <div className='project-viewer--header'>
        <input
            className='tag-search' 
            placeholder='Search tags...' 
            type='search'
            autoComplete='off'
            onFocus={function() {
              setShowTags(true);
            }}
            onBlur={function() {
              setShowTags(false);
            }}
            onChange={ onTagSearch }
        >
        </input>
        <div 
          className='project-viewer--tag-group'
          onMouseDown={ preventDefaultOnMouseDown }
        >
          {
            showTags &&
            <TagSelector
              tags={ tags }
              onTagClick={ onTagClick }
            >
            </TagSelector>
          }
          <TagList
            tags={ tags }
            onTagRemoveClick={ onTagRemoveClick }
            onTagsClear={ onTagsClear }
          >
          </TagList>
        </div>
      </div>
      <ProjectList
        tags={ tags }
        projects={ projects }
        onProjectSortChange={ onProjectSortChange }
      >
      </ProjectList>
    </div>
  );
}

export default ProjectViewer;