import { getPublicImageUrl } from "../../util/asset";

function Tag({ tag, shouldShow, onTagRemoveClick, isRemovable }) {
  const { name, icon } = tag;
  return (
    <>
      {
        shouldShow &&
        <div className='tag'>
          {
            icon &&
            <div className='tag--icon-wrapper'>
              <img className='tag--icon' src={ icon }></img>
            </div>
          }
          <div className='tag--name'>
            { name }
          </div>
          {
            isRemovable &&
            <div 
              className='tag--cross-wrapper'
              onClick={function() {
                onTagRemoveClick(tag);
              }}
            >
              <img className='tag--cross' src={ getPublicImageUrl('cross.svg') }></img>
            </div>
          }
        </div>
      }
    </>
  )
}

export default Tag;