import { useEffect, useRef } from 'react';
import { isElementOverflow } from '../../util/dom';
import Tag from './Tag';
import Tooltip from './Tooltip';

function ProjectItem({ tags, project }) {
  const contentDescriptionDiv = useRef(null);
  const { icon, title, description, demo, repo, tagList, isInFilter, date } = project;
  const year = new Date(date * 1000).getFullYear();

  useEffect(function() {
    const { current } = contentDescriptionDiv;
    // If element is visible (within tag filter)
    if (current) {
      const isOverflow = isElementOverflow(current);
      current.classList.toggle('overflow', isOverflow);
    }
  });

  return (
    <>
      {
        isInFilter &&
        <div className='project-item'>
          <div className='project-item--icon-wrapper'>
            <img 
              className='project-item--icon'
              src={ icon }
            >
            </img>
          </div>
          <div className='project-item--content'>
            <div className='project-item--content-title-group'>
              <div className='project-item--content-title'>
                { title }
              </div>
              <div className='project-item--content-year'>
                { year }
              </div>
            </div>
            <div className='project-item--content-description-group'>
              <div className='project-item--content-description' ref={ contentDescriptionDiv }>
                { description }
              </div>
              <Tooltip
                text={ description }
              >
              </Tooltip>
            </div>
            <div className='project-item--content-tags'>
              { 
                tags.map(function(tag, index) {
                  const { key } = tag;
                  const shouldShow = tagList?.includes(key);
                  return (
                    <Tag
                      tag={ tag }
                      shouldShow={ shouldShow }
                      isRemovable={ false }
                      key={ index }
                    > 
                    </Tag>
                  );
                })
              }
            </div>
          </div>
          <div className='project-item--cta-group'>
            {
              demo &&
              <a href={ demo } target='_blank' rel='noreferrer'>
                <div className='project-item--cta-demo'>
                  Demo
                </div>
              </a>
            }
            {
              repo &&
              <a href={ repo } target='_blank' rel='noreferrer'>
                <div className='project-item--cta-repo'>
                  Git
                </div>
              </a>
            }
          </div>
        </div>
      }
    </>
  )
}

export default ProjectItem;