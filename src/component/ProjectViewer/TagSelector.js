import TagSelectorItem from './TagSelectorItem';

function TagSelector({ tags, onTagClick }) {
  return (
    <div className='tag-selector'>
      {
        tags.map(function (tag, index) {
          return (
            <TagSelectorItem
              tag={ tag }
              onTagClick={ onTagClick }
              key={ index }
            >
            </TagSelectorItem>
          )
        })
      }
    </div>
  );
}

export default TagSelector;