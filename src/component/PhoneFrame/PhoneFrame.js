function PhoneFrame({ children }) {
  return (
    <div className='phone-frame'>
      <div className='phone-frame--mask'>
        <div className='phone-frame--inner'>
          { children }
        </div>
      </div>
    </div>
  );
}

export default PhoneFrame;