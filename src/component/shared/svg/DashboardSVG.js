function DashboardSVG() {
  return (
    <svg 
      xmlns="http://www.w3.org/2000/svg" 
      width="21" 
      height="21" 
      viewBox="0 0 42 42" 
      class="dashboard-svg"
    >
      <g fill="none" stroke="currentcolor" stroke-width="2.5"><rect width="16.75" height="16.75" x="1.25" y="1.25" rx="4"></rect><rect width="16.75" height="16.75" x="1.25" y="24" rx="4"></rect><rect width="16.75" height="16.75" x="24" y="1.25" rx="4"></rect><rect width="16.75" height="16.75" x="24" y="24" rx="4"></rect></g>
    </svg>
  );
}

export default DashboardSVG;