function QRCode({ img }) {
  return (
    <div className='qrcode'>
      <img className='qrcode--image' src={ img }></img>
    </div>
  )
}

export default QRCode;