import './App.css';
import PhoneFrame from './component/PhoneFrame/PhoneFrame';
import ProjectViewer from './component/ProjectViewer/ProjectViewer';
import NavigationBar from './component/NavigationBar/NavigationBar';
import QRCode from './component/shared/QRCode';
import { getPublicImageUrl } from './util/asset';

function App() {
  return (
    <div className="App">
      <PhoneFrame>
        <ProjectViewer>
        </ProjectViewer>
        <NavigationBar>
        </NavigationBar>
      </PhoneFrame>
      <div className='display--qrcode-wrapper'>
        <QRCode
          img={ getPublicImageUrl('qrcode.png') }
        >
        </QRCode>
      </div>
    </div>
  );
}

export default App;
