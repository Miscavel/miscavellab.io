export function getPublicAssetUrl(path) {
  return `./assets/${path}`;
}

export function getPublicImageUrl(img) {
  return getPublicAssetUrl(`images/${img}`);
}

export function getProjectImageUrl(img) {
  return getPublicImageUrl(`projects/${img}`);
}